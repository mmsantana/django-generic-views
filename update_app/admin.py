from django.contrib import admin
from .models import ClientDetails


class ClientAdmin(admin.ModelAdmin):
    fields = ['nome_do_cliente',
              'data_de_nascimento',
              'data_de_coleta',
              'nome_do_medico',
              'codigo_identificador']
    list_display = ['nome_do_cliente',
                    'data_de_nascimento',
                    'data_de_coleta',
                    'nome_do_medico',
                    'codigo_identificador']


admin.site.register(ClientDetails, ClientAdmin)
