from django.test import TestCase
from django.urls import reverse
from update_app.models import ClientDetails
import datetime


class TestViews(TestCase):
    def test_list_items_GET(self):
        response = self.client.get(reverse('update_app:list_items'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'update_app/list_items.html')

    def test_update_items(self):
        client = ClientDetails.objects.create(nome_do_cliente='Jarbas',
                                              data_de_nascimento='1966-02-10',
                                              data_de_coleta='2015-03-05',
                                              nome_do_medico='Médico Adriano',
                                              codigo_identificador=3219846349)

        response = self.client.post(
            reverse('update_app:update_items', kwargs={'pk': client.id}),
            {'nome_do_cliente': 'Juliana',
             'data_de_nascimento': '2000-02-10',
             'data_de_coleta': '2019-03-05',
             'nome_do_medico': 'Médico Carlos',
             'codigo_identificador': 6984984965}
        )

        response_pk = self.client.post(
            reverse('update_app:update_items', kwargs={'pk': 543}),
            {'nome_do_cliente': 'Jarbas',
             'data_de_nascimento': '1966-02-10',
             'data_de_coleta': '2015-03-05',
             'nome_do_medico': 'Médico Adriano',
             'codigo_identificador': 3219846349})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response_pk.status_code, 404)

        client.refresh_from_db()
        self.assertEqual(client.nome_do_cliente, 'Juliana')
        self.assertEqual(client.data_de_nascimento, datetime.date(2000, 2, 10))
        self.assertEqual(client.data_de_coleta, datetime.date(2019, 3, 5))
        self.assertEqual(client.nome_do_medico, 'Médico Carlos')
        self.assertEqual(client.codigo_identificador, 6984984965)

    def test_create_items(self):
        response = self.client.post(reverse('update_app:create_item'),
                                    {'nome_do_cliente': 'Jarbas',
                                     'data_de_nascimento': '1966-02-10',
                                     'data_de_coleta': '2015-03-05',
                                     'nome_do_medico': 'Médico Adriano',
                                     'codigo_identificador': 3219846349})
        client = ClientDetails.objects.last()

        self.assertEqual(response.status_code, 302)

        client.refresh_from_db()
        self.assertEqual(client.nome_do_cliente, 'Jarbas')
        self.assertEqual(client.data_de_nascimento, datetime.date(1966, 2, 10))
        self.assertEqual(client.data_de_coleta, datetime.date(2015, 3, 5))
        self.assertEqual(client.nome_do_medico, 'Médico Adriano')
        self.assertEqual(client.codigo_identificador, 3219846349)
