from django.test import TestCase
from update_app.models import ClientDetails
import datetime


class TestModels(TestCase):
    def test_model_client_details(self):
        client = ClientDetails.objects.create(nome_do_cliente='Jarbas',
                                              data_de_nascimento='1966-02-10',
                                              data_de_coleta='2015-03-05',
                                              nome_do_medico='Médico Adriano',
                                              codigo_identificador=3219846349)

        client.refresh_from_db()
        self.assertEqual(client.nome_do_cliente, 'Jarbas')
        self.assertEqual(client.data_de_nascimento, datetime.date(1966, 2, 10))
        self.assertEqual(client.data_de_coleta, datetime.date(2015, 3, 5))
        self.assertEqual(client.nome_do_medico, 'Médico Adriano')
        self.assertEqual(client.codigo_identificador, 3219846349)
