from django.db import models


class ClientDetails(models.Model):
    nome_do_cliente = models.CharField(max_length=256)
    data_de_nascimento = models.DateField()
    data_de_coleta = models.DateField()
    nome_do_medico = models.CharField(max_length=256)
    codigo_identificador = models.IntegerField()
