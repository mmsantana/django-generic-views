from django.urls import path
from . import views

app_name = 'update_app'

urlpatterns = [
    path('', views.ListItems.as_view(), name="list_items"),
    path('update/<pk>', views.UpdateItems.as_view(), name="update_items"),
    path('create/', views.CreateItem.as_view(), name="create_item")
]
