from django.views.generic.edit import UpdateView, CreateView
from .models import ClientDetails
from django.urls import reverse_lazy
from django.views.generic import ListView


class CreateItem(CreateView):
    model = ClientDetails
    fields = '__all__'
    template_name = 'update_app/create_item.html'
    success_url = reverse_lazy('update_app:list_items')


class UpdateItems(UpdateView):
    model = ClientDetails
    context_object_name = 'product_list'
    template_name = 'update_app/update_items.html'
    fields = ['nome_do_cliente',
              'data_de_nascimento',
              'data_de_coleta',
              'nome_do_medico',
              'codigo_identificador']
    success_url = reverse_lazy('update_app:list_items')


class ListItems(ListView):
    model = ClientDetails
    context_object_name = 'product_list'
    template_name = 'update_app/list_items.html'

