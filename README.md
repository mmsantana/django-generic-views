# Django Generic Views

Projeto simples para demonstrar a funcionalidade das generic views no Django.

Foram criadas as seguintes views:

	- CreateView
	
	- ListView
	
	- UpdateView
	
**Foram realizados testes unitários para cada view.